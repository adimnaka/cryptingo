<?php
class Database{
  private $host = "localhost";
  private $db_name = "cryptingo";
  private $user_name = "root";
  private $passwd = "";
  public $conn;
  
  //db connection
  public function getConnection()
  {
      // # code...
      $this->conn = null;
      try {
          $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->user_name, $this->passwd);
          //code...
      } catch (PDOException $exception) {
          //throw $th;
          echo "connection error: " . $exception->getMesssage();
      }
      return $this->conn;
  }
}
 
?>