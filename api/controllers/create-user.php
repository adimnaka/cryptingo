<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/cryptingo/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// database connection will be here
// files needed to connect to database
include_once './../config/database.php';
include_once './../models/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
// instantiate product object
$user = new User($db);
 
// submitted data will be here// get posted data
$data = json_decode(file_get_contents("php://input"));
 //print_r($data);
 $datr = file_get_contents("php://input"); 
 //$datr = parse_str($datr);

// set product property values
if ($data) {
    // print_r($data);
    $user->firstname = isset($data->firstname) ? $data->firstname : "";
    $user->lastname = isset($data->lastname) ? $data->lastname : "";
    $user->email = isset($data->email) ? $data->email : "";
    $user->password = isset($data->password) ? $data->password : "";
}
if (isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['password'])) {
    $user->firstname = isset($_POST['firstname']) ? $_POST['firstname'] : "";
    $user->lastname = isset($_POST['lastname']) ? $_POST['lastname'] : "";
    $user->email = isset($_POST['email']) ? $_POST['email'] : "";
    $user->password = isset($_POST['password']) ? $_POST['password'] : "";
}

// use the create() method here
// create the user
if(
    !empty($user->firstname) &&
    !empty($user->email) &&
    !empty($user->password) &&
    $user->create()
){
 
    // set response code
    http_response_code(200);
 
    // display message: user was created
    //header ("Location: http://localhost/cryptingo/view/login.php?success=1");
    //die();
    echo json_encode(array("message" => "User was created.", "data" => $user ));
}
 
// message if unable to create user
else{
 
    // set response code
    http_response_code(400);
    // header ("Location: http://localhost/cryptingo/view/register.php?success=2&error=Could%20not%20create%20account");
 
    // display message: unable to create user and display empty fields
    echo json_encode(array("message" => "Unable to create user.", "data" => $data));
}

?>