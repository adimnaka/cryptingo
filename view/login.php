<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Ko">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Login</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/my-login.css">
	<link rel="apple-touch-icon" href="theme-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="theme-assets/images/ico/favicon.ico">
    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,500,700" rel="stylesheet">
    <!--Font icons-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/fonts/themify/style.min.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/vendors/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/vendors/flipclock/flipclock.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/vendors/swiper/css/swiper.min.css">
    <!-- END VENDOR CSS-->
    <!-- END CRYPTO CSS-->
    <!-- BEGIN Page Level CSS-->
   <link rel="stylesheet" type="text/css" href="theme-assets/css/template-3d-animation.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
</head>

<body class="my-login-page "  >
	<header class="page-header">
      <!-- Horizontal Menu Start-->
      <nav class="main-menu static-top navbar-dark navbar navbar-expand-lg fixed-top mb-1"><div class="container">
    <a class="navbar-brand animated" data-animation="fadeInDown" data-animation-delay="1s" href="#head-area"><img src="theme-assets/images/logo.png" alt="Crypto Logo"/><span class="brand-text"><span class="font-weight-bold">Cryptingo </span>Invest</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div id="navigation" class="navbar-nav ml-auto">
            <ul class="navbar-nav mt-1">
                <li class="nav-item animated" data-animation="fadeInDown" data-animation-delay="1.1s">
                    <a class="nav-link" href="index.html#about">What is cryptingo</a>
                </li>
                <li class="nav-item animated" data-animation="fadeInDown" data-animation-delay="1.2s">
                    <a class="nav-link" href="index.html#problem-solution">Solutions</a>
                </li>
                <li class="nav-item animated" data-animation="fadeInDown" data-animation-delay="1.3s">
                    <a class="nav-link" href="index.html#whitepaper">Whitepaper</a>
                </li>
                <li class="nav-item animated" data-animation="fadeInDown" data-animation-delay="1.4s">
                    <a class="nav-link" href="index.html#token-sale-mobile-app">Invest</a>
                </li>
                <li class="nav-item animated" data-animation="fadeInDown" data-animation-delay="1.5s">
                    <a class="nav-link" href="index.html#roadmap">Roadmap</a>
                </li>
                <li class="dropdown show mr-2 px-2 animated" data-animation="fadeInDown" data-animation-delay="1.6s">
                    <a class="dropdown-toggle white" href="#" role="button" id="more" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
                    <div class="dropdown-menu" aria-labelledby="more">
                        <a class="dropdown-item" href="index.html#mobile-app">App</a>
                        <a class="dropdown-item" href="index.html#team">Team</a>
                        <a class="dropdown-item" href="index.html#faq">FAQ</a>
                        <a class="dropdown-item" href="index.html#contact">Contact</a>
                        <a class="dropdown-item" href="template-404.html">404</a>
                        <a class="dropdown-item" href="template-inner-page-with-sidebar.html">Sample Page</a>
                    </div>
                </li>
                <li class="dropdown show mr-4 animated" data-animation="fadeInDown" data-animation-delay="1.7s">
                    <a class="dropdown-toggle" href="#" role="button" id="language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flag-icon flag-icon-us"></span> EN</a>
                    <div class="dropdown-menu" aria-labelledby="language">
                        <a class="dropdown-item" href="#"><span class="flag-icon flag-icon-de"></span> GN</a>
                        <a class="dropdown-item" href="#"><span class="flag-icon flag-icon-es"></span> SP</a>
                        <a class="dropdown-item" href="#"><span class="flag-icon flag-icon-mq"></span> FR</a>
                    </div>
                </li>
            </ul>
            <span id="slide-line"></span>
            <form class="form-inline mt-2 mt-md-0">
                <a class="btn btn-sm btn-gradient-purple btn-glow my-2 my-sm-0 animated" data-animation="fadeInDown" data-animation-delay="1.8s" href="#" target="_blank">Sign in</a>
            </form>
        </div>
    </div>
</div>
      </nav>
      <!-- /Horizontal Menu End-->
    </header>
	<section class="h-100">
		<div class="container h-100 align-self-lg-center">
      <!-- /Display flash messages-->
        <div class="my-2"></div>
			<div class="row justify-content-md-center h-100 ">
            
            <div class="my-2"></div>
        <div class="my-2"></div>
            <div class="card-wrapper " >
            <div class="card fat " id="sample" hidden >	
						<div class="card-body " >
							<h4 class="card-title" id="sample-text" hidden >You are Logged IN</h4>
                            <h4 class="card-title" id="sample-text2" hidden >Wrong Credentials! try again</h4>
                        </div>
                        </div>
        </div>
				<div class="card-wrapper align-self-center ">
					
					<div class="card fat">
						<div class="card-body ">
							<h4 class="card-title">Login</h4>
							<form class="my-login-validation" id="loginform" novalidate="">
								<div class="form-group">
									<label for="email">E-Mail Address</label>
									<input id="email" type="email" class="form-control text-dark" name="email" value="" required autofocus>
									<div class="invalid-feedback">
										Email is invalid
									</div>
								</div>

								<div class="form-group">
									<label for="password">Password
										<a href="forgot.html" class="float-right">
											Forgot Password?
										</a>
									</label>
									<input id="password" type="password" class="form-control text-dark" name="password" required data-eye>
								    <div class="invalid-feedback">
								    	Password is required
							    	</div>
								</div>

								<div class="form-group row">
									<div class="col-sm-10 ">
										<label class="form-check-label"><input type="checkbox"> Remember me</label>
									</div>
								</div>

								<div class="form-group m-0" >
									<button type="submit" class="btn btn-primary btn-block">
										Login
									</button>
								</div>
								<div class="mt-4 text-center">
									Don't have an account? <a href="register.php">Create One</a>
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
<!-- jQuery & Bootstrap 4 JavaScript libraries -->
!-- jQuery & Bootstrap 4 JavaScript libraries -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


<script>
    setTimeout(function() {
    $('#sample').fadeOut('slow');}, 5000);

// function to make form values to json format
$.fn.serializeObject = function(){
 var o = {};
 var a = this.serializeArray();
 $.each(a, function() {
     if (o[this.name] !== undefined) {
         if (!o[this.name].push) {
             o[this.name] = [o[this.name]];
         }
         o[this.name].push(this.value || '');
     } else {
         o[this.name] = this.value || '';
     }
 });
 return o;
};

    // trigger when login form is submitted
 $(document).on('submit', '#loginform', function(){
   event.preventDefault();

    // get form data
    var sign_up_form=$(this);
    var form_data=JSON.stringify(sign_up_form.serializeObject());
    console.log(form_data);

$.ajax({
    url: "http://localhost/cryptingo/api/controllers/login.php",
    type : "POST",
    contentType: "application/json; charset=UTF-8",
    data : form_data,
    dataType: "text",
    success : function(result){
        // store jwt to cookie
        setCookie("jwt", result.jwt, 1);
        // show home page & tell the user it was a successful login
        // showHomePage();
        //$('#response').html("<div class='alert alert-success'>Successful login.</div>");console.log(result);
        // setInterval('location.reload()', 5000);
        $('#sample').attr('hidden', false);
        $('#sample-text').attr('hidden', false);
        setTimeout(function() {
        $('#sample').fadeOut('slow');}, 7000);
        $("html, body").animate({ scrollTop: 0 }, 200);
        
    },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        //setInterval('location.reload()', 10000);
            //location.reload()
            $('#sample').attr('hidden', false);
            $('#sample-text2').attr('hidden', false);
            setTimeout(function() {
            $('#sample').fadeOut('slow');}, 7000);
            $("html, body").animate({ scrollTop: 0 }, 200);
            
        }
});

 return false;
});

// show home page
function showHomePage(){
 
 // validate jwt to verify access
 var jwt = getCookie('jwt');
 $.post("http://localhost/cryptingo/api/controllers/validate-token.php", JSON.stringify({ jwt:jwt })).done(function(result) {

     // home page html will be here
 })

 // show login page on error will be here
}

// getCookie() will be here

// showLoggedInMenu() will be here
</script>
 <script>
window.onload = function() {
  loginPageFunction();
};

// show login page
function loginPageFunction(){
 
 // remove jwt
 setCookie("jwt", "", 1);
 clearResponse();
 showLoggedOutMenu();
}

// setCookie() will be here 
// function to set cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"+ ";SameSite=None"+ ";Secure";
}

// showLoggedOutMenu() will be here
</script>


</body>
</html>
